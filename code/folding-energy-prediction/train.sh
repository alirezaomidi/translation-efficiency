#!/bin/bash

pythonfile=$1
model=$2
epochs=150
batch_size=32

mkdir -p outputs/$model && echo "created directory outputs/$model" || echo "cannot create directory outputs/$model"

python -u $pythonfile data/energy-dataset.csv --model outputs/$model/model.hdf5 --output outputs/$model/ --tensorboard outputs/tblog/$model/ --batch-size $batch_size --epochs $epochs 1> outputs/$model/out.log 2> outputs/$model/err.log
