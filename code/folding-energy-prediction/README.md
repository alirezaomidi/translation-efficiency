# Predicting Minimum Folding Energy of a mRNA by its sequence

After a RNA is synthesized, it gets folded in the space. Predicting its secondary structure is an interesting task. Some tools are already developed that predict the secondary structure of an RNA, e.g. [ViennaRNA](https://www.tbi.univie.ac.at/RNA/). Here we use a neural network model to infer structural features from the sequence of an RNA, instead of just predicting the secondary structure. This may help us when we need structural features for our job. In this case, secondary structure is not actually a feature, it is the final answer to the question "What is the final folding of an RNA?".

## Fixed Dilated CNN
