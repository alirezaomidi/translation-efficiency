import numpy as np
import pandas as pd
import argparse
import os
import matplotlib.pyplot as plt
import tensorflow as tf
import keras
from keras.backend.tensorflow_backend import set_session
from keras.models import Sequential, load_model
from keras.layers import Dense, BatchNormalization, ReLU, Conv1D, MaxPool1D, GlobalMaxPool1D, Flatten, Dropout
from keras.datasets import mnist
from keras.optimizers import Nadam
from keras.callbacks import EarlyStopping, ModelCheckpoint
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from utils import r_square, TrainValTensorBoard


def main():
    # parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument('dataset', type=str, help='path to the dataset file')
    ap.add_argument('--model', type=str, default='model.hdf5', help='path to output trained model')
    ap.add_argument('--pretrained', type=str, help='path to the pretrained model if available')
    ap.add_argument('--output', type=str, default='output/', help='path to output results')
    ap.add_argument('--tensorboard', type=str, default='log/', help='path to save tensorboard logs')
    ap.add_argument('--epochs', type=int, default=50, help='number of epochs')
    ap.add_argument('--batch-size', type=int, default=32, help='the batch size used in the optimizer')
    args = ap.parse_args()

    # set this script to use a fraction of GPU's memory
    config = tf.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 0.5
    set_session(tf.Session(config=config))

    # prepare the data
    rna_alphabet = 'A C G T'.split()
    rna_size = 50

    data = pd.read_csv(args.dataset, header=None)

    x = np.array([list(i) for i in data[0].values])
    y = data[1].values.reshape(-1, 1)

    # one hot encode the data
    x_all = np.array([list(i * rna_size) for i in rna_alphabet])
    one_hot_encoder = OneHotEncoder(sparse=False)
    one_hot_encoder.fit(x_all)
    x = one_hot_encoder.transform(x).reshape(-1, rna_size, len(rna_alphabet))
    # split into train/test
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.2)
    # normalize the data
    for i in range(x.shape[2]):
        scaler = StandardScaler().fit(x_train[:, :, i])
        x_train[:, :, i] = scaler.transform(x_train[:, :, i])
        x_test[:, :, i] = scaler.transform(x_test[:, :, i])

    # define the model
    if args.pretrained:
        model = load_model(args.pretrained)
    else:
        model = Sequential([
            Conv1D(128, 11, input_shape=(*x_train.shape[1:],)),
            ReLU(),
            BatchNormalization(),
            MaxPool1D(2),
            Dropout(.2),

            Conv1D(128, 11),
            ReLU(),
            BatchNormalization(),
            MaxPool1D(2),
            Dropout(.2),

            Flatten(),

            Dense(64, use_bias=False),
            BatchNormalization(),
            ReLU(),
            Dropout(.2),
            
            Dense(1, use_bias=False),
            BatchNormalization()
        ])
        model.compile(loss='mse', optimizer='nadam', metrics=[r_square])

    model.summary()

    # keras callbacks
    tensorboard = TrainValTensorBoard(log_dir=args.tensorboard)
    earlystopping = EarlyStopping('val_r_square', mode='max', patience=10, verbose=1)
    checkpoint = ModelCheckpoint(args.model, monitor='val_r_square', mode='max',
                                verbose=0, save_best_only=True, period=1)

    # train the model
    history = model.fit(x_train, y_train, batch_size=args.batch_size, epochs=args.epochs,
                        validation_split=.2, verbose=2, callbacks=[tensorboard, earlystopping, checkpoint])


if __name__ == "__main__":
    main()