from Bio import SeqIO
import re
from datetime import datetime, timedelta


def fasta_to_dataframe(fasta_path):
    sequences = SeqIO.parse(open(fasta_path),'fasta')
    return [(i.id, str(i.seq)) for i in sequences]


def parse_time_limit(time_limit):
    regex = r'^(?!$)(?:(?P<days>\d+)d)?(?:(?P<hours>\d+)h)?(?:(?P<minutes>\d+)m)?$'
    time_limit = re.match(regex, time_limit)
    if time_limit is not None:
        days = int(time_limit.group('days')) if time_limit.group('days') is not None else 0
        hours = int(time_limit.group('hours')) if time_limit.group('hours') is not None else 0
        minutes = int(time_limit.group('minutes')) if time_limit.group('minutes') is not None else 0
        time_limit = datetime.now() + timedelta(days=days, hours=hours, minutes=minutes)
        time_limit = time_limit.strftime('%Y-%m-%d %H:%M')
    return time_limit 