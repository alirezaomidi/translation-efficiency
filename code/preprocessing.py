import numpy as np
import pandas as pd
import requests
import os
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split


def prepare_te_data(mrna_path, te_path, sample_name, max_length=None, codon_types=True, aa_types=True, rnaseq=True,
                    test_split=.1, validation_split=.1, te_col_name='CN34_S5_r1_te.h.trim.uncontam', random_seed=42):

    te = pd.read_csv(te_path, index_col='transcript')[sample_name]
    te = pd.DataFrame(te)
    te = te.rename(columns={te_col_name: 'te'})
    te = np.log1p(te)
    
    # configure features the user wants to put in the data
    cat_features, num_features = ['nucleotides'], []
    if codon_types:
        cat_features.append('codon_type')
    if aa_types:
        cat_features.append('aa_type')
    if rnaseq:
        num_features.append('rnaseq')

    # load mRNA sequences
    mrnas_raw = pd.read_csv(mrna_path, index_col='transcript')
    mrnas_cat = mrnas_raw[cat_features].groupby('transcript').agg(lambda x: ' '.join(x))
    if num_features:
        mrnas_num = mrnas_raw[num_features].groupby('transcript').agg(list)
        mrnas = pd.concat([mrnas_cat, mrnas_num], axis=1)
    else:
        mrnas = mrnas_cat
    mrnas = mrnas.join(te, on='transcript', how='right')

    # normalize
    for feat in num_features:
        mrnas[feat] = mrnas[feat].apply(lambda x: np.log1p(np.array(x)))

    # filter out long mRNAs
    if max_length is None:
        short_mrnas = mrnas
    else:
        short_mrnas = mrnas[mrnas['nucleotides'].apply(lambda x: len(x.split())) <= max_length]

    x, y = short_mrnas.drop('te', axis=1), short_mrnas['te']

    # tokenize categorical columns
    word_indices = {}
    for key in cat_features:
        tokenizer = Tokenizer()
        tokenizer.fit_on_texts(x[key])
        word_indices[key] = tokenizer.word_index
        x[key] = tokenizer.texts_to_sequences(x[key])
    
    # train/val/test split
    x_train, y_train, x_val, y_val, x_test, y_test = train_val_test_split(x, y, validation_size=validation_split, test_size=test_split,
                                                                          random_seed=random_seed)
    return x_train, y_train, x_val, y_val, x_test, y_test, word_indices


def train_val_test_split(x, y, test_size=.1, validation_size=.1, sort_by_len='nucleotides', random_seed=42):
    # train/val/test split
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=test_size, random_state=random_seed)
    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=validation_size, random_state=random_seed)

    if sort_by_len is not None:
        # sort mRNAs by length
        train_sorted_indices = x_train[sort_by_len].apply(len).argsort()
        test_sorted_indices = x_test[sort_by_len].apply(len).argsort()
        val_sorted_indices = x_val[sort_by_len].apply(len).argsort()
        x_train, y_train = x_train.iloc[train_sorted_indices], y_train[train_sorted_indices]
        x_test, y_test = x_test.iloc[test_sorted_indices], y_test[test_sorted_indices]
        x_val, y_val = x_val.iloc[val_sorted_indices], y_val[val_sorted_indices]

    return x_train, y_train, x_val, y_val, x_test, y_test


def download_eclip_rbp(path):
    ENCODE_BASE_URL = 'https://www.encodeproject.org'
    ECLIP_URL = ENCODE_BASE_URL + '/search/?type=Experiment' + \
        '&status=released&internal_tags=ENCORE&assay_title=eCLIP' + \
        '&biosample_ontology.term_name=K562&assay_title=eCLIP&format=json'
    
    req = requests.get(ECLIP_URL)
    if not req.ok:
        return None

    os.makedirs(path, exist_ok=True)

    downloaded_files = []
    for protein in req.json()['@graph']:
        for f in protein['files']:
            url = f'{ENCODE_BASE_URL}{f["@id"]}?format=json'
            r = requests.get(url)
            if not r.ok or r.json()['file_type'] != 'bed narrowPeak' or r.json()['biological_replicates'] != [1, 2]:
                continue
            f = r.json()
            r = requests.get(f'{ENCODE_BASE_URL}{f["href"]}')
            if not r.ok:
                continue
            filepath = os.path.join(path, f'{f["accession"]}.bed.gz')
            open(filepath, 'wb').write(r.content)
            downloaded_files.append(filepath)
            print(f'Downloaded {f["@id"]}.')
    return downloaded_files
