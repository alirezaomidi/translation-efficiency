import os
import numpy as np
import pandas as pd
import tensorflow as tf
import keras.backend as K
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.preprocessing.text import Tokenizer
from keras.models import load_model
import argparse
from sklearn.model_selection import train_test_split
from datetime import datetime
import pickle
import talos
import json

import models
from preprocessing import prepare_te_data


def main():
    # parse the arguments
    ap = argparse.ArgumentParser()
    # ap.add_argument('dataset', type=str, help='path to the dataset file')
    ap.add_argument('--model', type=str, default='CNN', help='class name of the model to train')
    ap.add_argument('--pretrained', type=str, help='path to the pretrained model if available')
    ap.add_argument('--output', type=str, default='output/', help='path to the directory to save the results')
    ap.add_argument('--epochs', type=int, default=50, help='number of epochs')
    ap.add_argument('--batch-size', type=int, default=32, help='the batch size used in the optimizer')
    ap.add_argument('--random-seed', type=int, default=42, help='the random seed to use in train/val/test split (reproducibility feature)')
    ap.add_argument('--gpu', type=int, help='the GPU ID to train the model on (if GPU is available)')
    ap.add_argument('--model-params', type=str, help='path to the JSON file containing model parameters. ' + \
                                                     'If non provided, will use default parametrs of the required model')
    args = ap.parse_args()

    # configure the GPU
    if args.gpu is not None:
        os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu)
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        K.set_session(tf.Session(config=config))

    # create output directory
    output_dir = os.path.join(args.output, args.model.lower(), f'{datetime.now():%Y-%m-%d-%H-%M-%S}')
    os.makedirs(output_dir)

    # prepare the data
    DATA_DIR = '../data/'
    TE_PATH = os.path.join(DATA_DIR, 'csv', 'te_LMCN.v2.csv')
    TE_SAMPLE_NAME = 'CN34_S5_r1_te.h.trim.uncontam'
    RNA_PATH = os.path.join(DATA_DIR, 'csv', 'preprocessed.csv')

    x_train, y_train, x_val, y_val, x_test, y_test, word_indices = prepare_te_data(RNA_PATH, TE_PATH, TE_SAMPLE_NAME,
        max_length=6000, random_seed=args.random_seed)

    # define the model
    if args.pretrained:
        model = load_model(args.pretrained)
    else:
        vocab_lengths = (5, 66, 23, None)
        model = eval(f'models.{args.model}')(input_shape=(None, ), vocab_lengths=vocab_lengths)

    model.compile('adam', 'mse', metrics=[talos.utils.metrics.rmse])
    model.summary()

    # keras callbacks
    # tensorboard = TrainValTensorBoard(log_dir=args.tensorboard)
    earlystopping = EarlyStopping('val_loss', patience=max(50, args.epochs // 10), verbose=1)
    checkpoint = ModelCheckpoint(os.path.join(output_dir, 'model.h5'), monitor='val_loss',
                                 verbose=0, save_best_only=True, period=1)
    reduce_lr = ReduceLROnPlateau('val_loss', patience=max(40, args.epochs // 12), factor=0.1)
                            
    # train the model
    history = model.fit(x_train, y_train, batch_size=args.batch_size, epochs=args.epochs,
                        validation_data=(x_val, y_val), verbose=2, callbacks=[earlystopping, checkpoint, reduce_lr])
    
    # save the history and configurations
    pickle.dump(history, open(os.path.join(output_dir, 'history.pkl'), 'wb'))
    json.dump(vars(args), open(os.path.join(output_dir, 'arguments.json'), 'w'), indent=2)
    
            

if __name__ == "__main__":
    main()
