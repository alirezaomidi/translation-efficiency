import os
import shutil
import numpy as np
import pandas as pd
import tensorflow as tf
import keras.backend as K
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.preprocessing.text import Tokenizer
import argparse
from sklearn.model_selection import train_test_split
from datetime import datetime, timedelta
import json
import re
import talos
import pickle

import models
import utils
from preprocessing import prepare_te_data


def main():
    # parse the arguments
    ap = argparse.ArgumentParser()
    # ap.add_argument('--dataset', type=str, help='path to the dataset file')
    ap.add_argument('--model', type=str, default='CNN', help='class name of the model to train')
    ap.add_argument('--output', type=str, default='output/', help='path to the directory to save the results')
    ap.add_argument('--params', type=str, required=True, help='path to the JSON file containing hyper-optimization parameters.')
    ap.add_argument('--random-seed', type=int, default=42, help='the random seed to use in train/val/test split (reproducibility feature)')
    ap.add_argument('--gpu', type=int, help='the GPU ID to train the model on (if GPU is available)')
    ap.add_argument('--time-limit', type=str, help='allows setting a time limit for the whole process.' + \
                                                   'e.g "1d3h5m" means 1 day and 3 hours and 5 minutes in total')
    args = ap.parse_args()

    # configure the GPU
    if args.gpu is not None:
        os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu)

    # define the model
    model = eval(f'models.{args.model}').talos

    # parse time limit
    time_limit = None
    if args.time_limit is not None:
        time_limit = utils.parse_time_limit(args.time_limit)
        print(f'Time limit is set on {time_limit}')

    # create output directory
    output_dir = os.path.join(args.output, 'hyperoptim', f'{datetime.now():%Y-%m-%d-%H-%M-%S}')
    os.makedirs(output_dir)
    
    # read hyper-optimization parameters
    params = json.load(open(args.params))
    print(f'Params: {params}')

    # save the arguments and params in the output directory (for easier later analysis)
    json.dump(params, open(os.path.join(output_dir, 'params.json'), 'w'), indent=2)
    json.dump(vars(args), open(os.path.join(output_dir, 'arguments.json'), 'w'), indent=2)

    # prepare the data
    print('Loading data...')
    DATA_DIR = '../data/'
    TE_PATH = os.path.join(DATA_DIR, 'csv', 'te_LMCN.v2.csv')
    TE_SAMPLE_NAME = 'CN34_S5_r1_te.h.trim.uncontam'
    RNA_PATH = os.path.join(DATA_DIR, 'csv', 'preprocessed.csv')
    x_train, y_train, x_val, y_val, x_test, y_test, word_indices = prepare_te_data(RNA_PATH, TE_PATH, TE_SAMPLE_NAME,
        max_length=6000, random_seed=args.random_seed)
    print('Data loaded.')

    # train the model
    print('Starting hyperparameter optimization...')
    scan_object = talos.Scan(x=x_train,
                             y=y_train,
                             x_val=x_val,
                             y_val=y_val,
                             params=params,
                             model=model,
                             experiment_name=output_dir,
                             fraction_limit=.01,
                             random_method='uniform_mersenne',
                             reduction_method='correlation',
                             reduction_metric='val_rmse',
                             minimize_loss=True,
                             reduction_interval=20,
                             reduction_window=20,
                             reduction_threshold=0.2,
                             print_params=True,
                             time_limit=time_limit)
    
    # save the results
    print('Saving the result')
    talos_model_name = args.model.lower()
    talos.Deploy(scan_object, talos_model_name, 'rmse', asc=True)
    shutil.move(f'{talos_model_name}.zip', output_dir)
    print('Saved. Done.')


if __name__ == "__main__":
    main()
