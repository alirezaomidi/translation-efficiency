from keras.models import Model, Sequential
from keras.layers import Dense, Input, Conv1D, Embedding, Concatenate, MaxPool1D, BatchNormalization, Reshape, Dropout, GlobalAvgPool1D, \
                         Activation, SpatialDropout1D, Add
from keras.callbacks import EarlyStopping
from .utils import SequenceGenerator
import talos


class BPNet:

    def __init__(self, **params):
        self.model = self.create(**params)

    
    @classmethod
    def _conv1d(cls, x, n_filters, size, activation='relu', kernel_initializer='glorot_normal', padding='same', batch_norm=True, dilation=1):
        x = Conv1D(n_filters, size, padding=padding, kernel_initializer=kernel_initializer, dilation_rate=dilation)(x)
        if batch_norm:
            x = BatchNormalization()(x)
        x = Activation(activation)(x)
        return x

    
    @classmethod
    def _dense(cls, x, dim, activation='relu', kernel_initializer='glorot_normal', batch_norm=True, dropout=.5):
        x = Dense(dim, kernel_initializer=kernel_initializer)(x)
        if batch_norm:
            x = BatchNormalization()(x)
        x = Activation(activation)(x)
        if dropout > 0:
            x = Dropout(dropout)(x)
        return x


    @classmethod
    def create(cls,
               vocab_lengths,
               input_shape=(None,),
               output_dim=1,
               embedding_size=16,
               conv_filters=64,
               first_conv_size=25,
               inner_conv_size=3,
               conv_dropout=0,
               dense_dim=64,
               dense_dropout=.5,
               name='BPNet',
               **kwargs):
        # embed each input channel into a vector and concatenate them all
        xs = []
        embs = []
        for vlen in vocab_lengths:
            x = Input(shape=input_shape)
            xs.append(x)
            if vlen is not None:
                embs.append(Embedding(vlen, embedding_size)(x))
            else:
                embs.append(Reshape((-1, 1))(x))
        emb = Concatenate()(embs)

        # convolution layers
        conv = cls._conv1d(emb, conv_filters, first_conv_size, 'relu', padding='same', kernel_initializer='glorot_normal', batch_norm=False)
        resid = conv

        for i in range(1, 13):
            conv = cls._conv1d(resid, conv_filters, inner_conv_size, 'relu', padding='same', kernel_initializer='glorot_normal',
                               batch_norm=False, dilation=2**i)
            resid = Add()([conv, resid])
            
        conv = GlobalAvgPool1D()(resid)

        # dense layers
        dense = cls._dense(conv, dense_dim, 'relu', kernel_initializer='glorot_normal', batch_norm=True, dropout=dense_dropout)
        dense = cls._dense(dense, output_dim, 'linear', kernel_initializer='glorot_normal', batch_norm=False, dropout=0)

        return Model(inputs=xs, outputs=dense, name=name)


    @classmethod
    def talos(cls, x_train, y_train, x_val, y_val, params):
        model = cls.create(**params)

        model.compile(optimizer=params['optimizer'],
                      loss='mse',
                      metrics=[talos.utils.metrics.rmse])

        early_stopper = EarlyStopping('val_loss', min_delta=0, patience=params['epochs'] // 10,
                                      verbose=0, mode='auto', restore_best_weights=True)

        out = model.fit_generator(generator=SequenceGenerator(x_train, y_train, batch_size=params['batch_size']),
                                  validation_data=SequenceGenerator(x_val, y_val, batch_size=params['batch_size']),
                                  epochs=params['epochs'],
                                  verbose=0,
                                  callbacks=[early_stopper])
        return out, model


    def compile(self, *args, **kwargs):
        self.model.compile(*args, **kwargs)
        return self
    

    def fit(self, x, y, validation_data, batch_size=32, epochs=10, **kwargs):
        self.model.fit_generator(generator=SequenceGenerator(x, y, batch_size=batch_size),
                                 validation_data=SequenceGenerator(*validation_data, batch_size=batch_size),
                                 epochs=epochs,
                                 **kwargs)
        return self


    def summary(self, *args, **kwargs):
        return self.model.summary(*args, **kwargs)