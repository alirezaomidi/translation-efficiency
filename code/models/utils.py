import numpy as np
import keras
from keras.preprocessing.sequence import pad_sequences


class SequenceGenerator(keras.utils.Sequence):

    def __init__(self, x, y, batch_size=32, fix_max_len=None):
        self.x, self.y = x, y
        self.batch_size = batch_size
        self.fix_max_len = fix_max_len

    def __len__(self):
        return int(np.ceil(len(self.x) / float(self.batch_size)))

    def __getitem__(self, idx):
        batch_x = self.x.iloc[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_y = self.y.iloc[idx * self.batch_size:(idx + 1) * self.batch_size]
        
        if self.fix_max_len is not None:
            return [pad_sequences(batch_x[key], padding='pre', dtype='float64', maxlen=self.fix_max_len) for key in batch_x], batch_y
        else:
            return [pad_sequences(batch_x[key], padding='pre', dtype='float64') for key in batch_x], batch_y
