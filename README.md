# Identifying sequence and structural determinants of translation using deep learning

In this project, we try to identify the determinants of mRNA translation by:

1. Train a model on ribo-seq data and ask to predict a translation-related measure (e.g. translation efficiency of a mRNA)
2. Explore the representation which the model learns to be able to do a good job in predicting the measure
3. See how these representations are related to translation

## Translation Efficiency
Translation efficiency (TE) is measure of the rate a mRNA gets translated into its corresponding protein.

Many factors are currently believed that regulate TE:
- Codon usage
- mRNA structure
- miRNA bindings
- ...
